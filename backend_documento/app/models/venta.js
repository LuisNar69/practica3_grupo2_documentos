'use strict';

module.exports = (sequelize, DataTypes) => {
    const venta = sequelize.define('venta', {
        numero: { type: DataTypes.STRING(100), defaultValue: "NONE" },
        fecha: { type: DataTypes.DATE, defaultValue:  sequelize.literal('CURRENT_TIMESTAMP')},
        metodo_pago: { type: DataTypes.ENUM("TARJETA_DEBITO", "TARJETA_CREDITO", "EFECTIVO"), defaultValue: "TARJETA_CREDITO" },
        estado: { type: DataTypes.ENUM("PAGADA", "CANCELADA"), defaultValue: "CANCELADA" },
        total: { type: DataTypes.DECIMAL(15,2), defaultValue: 0.0 },
        copia_vendedor: { type: DataTypes.STRING(20), defaultValue: "" },
        
    }, { freezeTableName: true });
    venta.associate = function(models){
        venta.belongsTo(models.persona, {foreignKey: 'id_persona'});
        venta.hasMany(models.detalleVenta,{foreignKey: 'id_venta', as: 'detalleVenta'});
    }
    return venta;
};