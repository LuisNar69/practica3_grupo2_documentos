'use strict'

module.exports = (sequelize, DataTypes) => {

    const documento = sequelize.define('documento', {
        titulo: { type: DataTypes.STRING(150), defaultValue: "NONE" },
        autor: { type: DataTypes.STRING(150), defaultValue: "NONE" },
        isbn: { type: DataTypes.STRING(150), defaultValue: "NONE" },
        precio: { type: DataTypes.DECIMAL(10, 2), defaultValue: 0.0 },
        archivo: { type: DataTypes.STRING, defaultValue: "NONE" },
        tipo: { type: DataTypes.ENUM('TEXTO', 'AUDIO'), defaultValue: "TEXTO" },
        external_id: { type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4 },
        estado: { type: DataTypes.BOOLEAN, defaultValue: false },
        copia_identificacion: { type: DataTypes.STRING(20), defaultValue: "" },

    }, { freezeTableName: true });
    documento.associate = function (models) {
        documento.hasOne(models.detalleVenta, { foreignKey: 'id_documento', as: "detalleVenta" });
        documento.hasMany(models.fotoDocumento, { foreignKey: 'id_documento', as: "fotoDocumento" });
    }
    return documento;
}