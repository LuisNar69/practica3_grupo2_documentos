'use strict';

module.exports = (sequelize, DataTypes) => {
    const fotoDocumento = sequelize.define('fotoDocumento', {
        url: { type: DataTypes.STRING(100), defaultValue: "NO_DATA" },
        external_id: { type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4 },
    }, { freezeTableName: true });

    fotoDocumento.associate = function (models) {
        fotoDocumento.belongsTo(models.documento, { foreignKey: 'id_documento' });
    }
    return fotoDocumento;
};        