'use strict';

module.exports = (sequelize, DataTypes) => {
    const detalleVenta = sequelize.define('detalleVenta', {
        cantidad: { type: DataTypes.INTEGER, defaultValue: 0 },
        subtotal: { type: DataTypes.DECIMAL(15, 2), defaultValue: 0.0 },
        descuento: { type: DataTypes.DECIMAL(6, 2), defaultValue: 0.0 },
    }, { freezeTableName: true });
    detalleVenta.associate = function (models) {
        detalleVenta.belongsTo(models.documento, { foreignKey: 'id_documento' });
        detalleVenta.belongsTo(models.venta, { foreignKey: 'id_venta' });
    }
    return detalleVenta;
};