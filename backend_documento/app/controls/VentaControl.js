'use strict';
var models = require('../models');
var venta = models.venta;
var detalleVenta = models.detalleVenta;
var persona = models.persona;
var documento = models.documento;

class VentaControl {

    async listar(req, res) {
        var lista = await venta.findAll({
            include: [
                { model: models.detalleVenta, as: "detalleVenta", attributes: ['cantidad', 'subtotal', "descuento"] },
            ],
            attributes: ['numero', 'fecha', 'metodo_pago', 'estado', 'total']
        });
        res.status(200);
        res.json({ msg: "OK!", code: 200, info: lista });
    }

    async buscarVenta(req, res) {
        const vendedor = req.params.vendedor;

        var personaAux = await persona.findOne({
            where: { external_id: vendedor },
            attributes: ["nombres", "apellidos", "cedula", "direccion", "id"],
        });

        var lista = await venta.findAll({
            where: { copia_vendedor: personaAux.id },
            include: [
                { model: models.detalleVenta, as: "detalleVenta", attributes: ['cantidad', 'subtotal', "descuento"] },
            ],
            attributes: ['numero', 'fecha', 'metodo_pago', 'estado', 'total']
        });
        res.status(200);
        res.json({ msg: "OK!", code: 200, info: lista });
    }

    async guardar(req, res) {

        const ventas = JSON.parse(req.body.factura);
        const dataDesArray = [];

        if (Array.isArray(req.body.dataDes)) {
            for (const item of req.body.dataDes) {
                dataDesArray.push(JSON.parse(item));
            }
        } else {
            dataDesArray.push(JSON.parse(req.body.dataDes));
        }

        let persona_1 = ventas.external_cliente;
        let persona_2 = ventas.external_vendedor;



        if (persona_1 != undefined && persona_2 != undefined) {

            let personaAux = await persona.findOne({ where: { external_id: persona_1 } });
            let personaAux2 = await persona.findOne({ where: { external_id: persona_2 } });

            if (personaAux && personaAux2) {
                var data = {
                    numero: ventas.numero,
                    metodo_pago: ventas.metodo,
                    estado: ventas.estado,
                    total: ventas.total,
                    id_persona: personaAux.id,
                    copia_vendedor: personaAux2.id

                };

                let transaction = await models.sequelize.transaction();

                try {

                    const auxVenta = await venta.create(data, { transaction });

                    for (const doc of dataDesArray) {

                        let docAux = await documento.findOne({ where: { external_id: doc.external_id } });

                        docAux.copia_identificacion = personaAux.cedula;
                        docAux.estado = true;

                        var result = await docAux.save();

                        if (result === null) {
                            res.status(400);
                            res.json({ msg: "ERROR", tag: "error al agregar venta", code: 200 });
                        } else {
                            res.status(200);
                            let datos = {
                                cantidad: ventas.cantidad,
                                subtotal: ventas.subtotal,
                                id_venta: auxVenta.id,
                                id_documento: docAux.id,
                            };
                            await detalleVenta.create(datos, { transaction });
                        }
                    }
                    await transaction.commit();

                    res.json({ msg: "OK", tag: "Se ha registrado correctamente su venta", code: 200 });

                } catch (error) {
                    if (transaction) await transaction.rollback();
                    res.status(203);
                    res.json({ msg: "Error", code: 203, error_msg: error.errors[0].message });
                }
            } else {
                res.status(400);
                res.json({ msg: "Datos no encontrados", code: 400 });
            }
        } else {
            res.status(400);
            res.json({ msg: "Cliente o vendedor  no registrados", code: 400 });
        }
    }
}

module.exports = VentaControl;