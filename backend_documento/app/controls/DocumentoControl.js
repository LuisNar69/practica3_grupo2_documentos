'use strict'

var models = require('../models');
const fs = require('fs');
const { Op } = require('sequelize');
var fotoDocumento = models.fotoDocumento;
var documento = models.documento;
var detalleVenta = models.detalleVenta;
const moment = require('moment');

class DocumentoControl {

    async listar(req, res) {
        var lista = await documento.findAll({
            where: { estado: false },
            include: [
                { model: models.fotoDocumento, as: "fotoDocumento", attributes: ['url'] }
            ],
            attributes: ['titulo', 'autor', 'isbn', 'precio', 'estado', "tipo", "archivo", 'external_id']
        });
        res.status(200);
        res.json({ msg: "OK!", code: 200, info: lista });
    }

    async listarNoVendidos(req, res) {
        var lista = await documento.findAll({
            where: { estado: false },
            include: [
                { model: models.fotoDocumento, as: "fotoDocumento", attributes: ['url'] }
            ],
            attributes: ['titulo', 'autor', 'isbn', 'precio', 'estado', "tipo", "archivo", 'external_id']
        });
        res.status(200);
        res.json({ msg: "OK!", code: 200, info: lista });
    }

    async obtener(req, res) {
        const external = req.params.external;
        var lista = await documento.findOne({
            where: { external_id: external },
            attributes: ['titulo', 'autor', 'isbn', 'precio', 'estado', "tipo", "archivo", 'external_id']
        });
        if (lista === null) {
            lista = {};
        }
        res.status(200);
        res.json({ msg: "OK!", code: 200, info: lista });
    }

    async guardar(req, res) {

        const carData = JSON.parse(req.body.car);

        if (carData.hasOwnProperty('titulo') &&
            carData.hasOwnProperty('autor') &&
            carData.hasOwnProperty('isbn') &&
            carData.hasOwnProperty('precio') &&
            carData.hasOwnProperty('tipo')) {

            var uuid = require('uuid');

            var data = {
                titulo: carData.titulo,
                autor: carData.autor,
                isbn: carData.isbn,
                precio: carData.precio,
                tipo: carData.tipo,
                external_id: uuid.v4(),
            }

            let transaction = await models.sequelize.transaction();

            try {
                const aux = await documento.create(data, { transaction });

                const filesImg = req.files['img']; // Obtener los archivos correspondientes al campo 'img'

                if (filesImg) {
                    console.log(filesImg)
                    for (const file of filesImg) {
                        const imageUrl = `http://localhost:3006/images/${file.filename}`;

                        let datos = {
                            url: imageUrl,
                            id_documento: aux.id,
                            external_id: uuid.v4(),
                        };
                        await fotoDocumento.create(datos, { transaction });
                    }
                }

                await transaction.commit();

                const filesAudio = req.files['audio'];

                if (filesAudio) {
                    aux.archivo = `http://localhost:3006/multimedia/${filesAudio[0].filename}`;
                    aux.save()
                }

                res.status(200).json({ msg: "OK", tag: "Documento registrado correctamente", code: 200 });

            } catch (error) {
                await transaction.rollback();

                res.status(200).json({ msg: error.message, code: 200 });
            }
        } else {
            res.status(400);
            res.json({ msg: "Error", tag: "faltan datos", code: 400 });
        }
    }

    async modificar(req, res) {
        var doc = await documento.findOne({ where: { external_id: req.params.external } })
        const carData = JSON.parse(req.body.car);

        if (doc === null) {
            res.status(400);
            res.json({ msg: "Error", tag: "El dato a modificar no existe", code: 400 });
        } else {

            if (carData.hasOwnProperty('titulo') &&
                carData.hasOwnProperty('autor') &&
                carData.hasOwnProperty('isbn') &&
                carData.hasOwnProperty('precio')) {

                var uuid = require('uuid')
                const filesAudio = req.files["audio"];
                let archivo = "";
                console.log(doc.archivo)

                if (doc.archivo != "" && doc.archivo != null) {
                    archivo = doc.archivo.split("/").pop();
                }

                if (filesAudio != null || filesAudio != undefined) {
                    if (filesAudio[0].filename != archivo) {
                        fs.unlink('public/multimedia/' + archivo, (err) => {
                            if (err) {
                                console.error('Error al intentar eliminar el archivo:', err);
                            } else {
                                console.log('Archivo eliminado correctamente.');
                            }
                        });
                    }
                    doc.archivo = `http://localhost:3006/multimedia/${filesAudio[0].filename}`;
                } else if (carData.tipo === 'TEXTO') {

                    const filePath = 'public/multimedia/' + archivo;

                    fs.unlink(filePath, async (err) => {
                        if (err) {
                            console.error('Error al intentar eliminar el archivo:', err);
                        } else {
                            console.log('Archivo eliminado correctamente.');
                            // Establecer el campo archivo como null después de la eliminación exitosa
                        }
                    })
                    doc.archivo = "";
                }

                doc.titulo = carData.titulo;
                doc.autor = carData.autor;
                doc.isbn = carData.isbn;
                doc.precio = carData.precio;
                doc.tipo = carData.tipo;
                doc.external_id = uuid.v4();

                var result = await doc.save();

                if (result === null) {
                    res.status(400);
                    res.json({ msg: "Error", tag: "No se han modificado los datos", code: 400 });
                } else {
                    res.status(200);
                    res.json({ msg: "Success", tag: "Datos modificados correctamente", code: 200 });
                }
            } else {
                res.status(400);
                res.json({ msg: "Error", tag: "faltan datos", code: 400 });
            }
        }
    }

    async listarVendidos(req, res) {
        const lista = await detalleVenta.findAll({
            attributes: ['id_documento', 'createdAt']
        });

        let listado = [];

        if (lista.length > 0) {
            const idsDocumentos = lista.map(item => item.id_documento);
            const quincenaActual = Math.ceil(moment().date() / 15);

            listado = await documento.findAll({
                where: {
                    id: { [Op.in]: idsDocumentos },
                    createdAt: {
                        [Op.between]: [
                            moment().set('date', quincenaActual * 15 - 14).startOf('day').toDate(),
                            moment().set('date', quincenaActual * 15).endOf('day').toDate()]
                    }
                },
                include: [
                    { model: models.fotoDocumento, as: "fotoDocumento", attributes: ['url'] }
                ],
                attributes: ['titulo', 'autor', 'isbn', 'precio', 'estado', 'tipo', 'archivo', 'external_id']
            });
        }

        res.status(200)
        res.json({ msg: "OK!", code: 200, info: listado });
    }
}

module.exports = DocumentoControl;