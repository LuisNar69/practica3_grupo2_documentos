'use strict'

var models = require('../models');
var persona = models.persona;
var rol = models.rol;
var cuenta = models.cuenta;
const bcrypt = require('bcrypt');
const saltRounds = 8;

class PersonaControl {

    async listar(req, res) {
        var lista = await persona.findAll({
            attributes: ["nombres", "apellidos", "cedula", "direccion", ["external_id", "id"]],
            include: [
                { model: models.cuenta, as: "cuenta", attributes: ["usuario"] },
                { model: models.rol, as: "rol", attributes: ["nombre"], where: {   nombre: "Cliente" } },
            ]
        });
        res.status(200);
        res.json({ msg: "OK", code: 200, datos: lista });
    }

    async listarVendedor(req, res) {
        var lista = await persona.findAll({
            attributes: ["nombres", "apellidos", "cedula", "direccion", ["external_id", "id"]],
            include: [
                { model: models.cuenta, as: "cuenta", attributes: ["usuario"] },
                { model: models.rol, as: "rol", attributes: ["nombre"], where: {   nombre: "Vendedor" } },
            ]
        });
        res.status(200);
        res.json({ msg: "OK", code: 200, datos: lista });
    }

    async obtener(req, res) {
        const external = req.params.external;
        var lista = await persona.findOne({
            where: { external_id: external },
            attributes: ["nombres", "apellidos", "cedula", "direccion", ["external_id", "id"]],
            include: [
                { model: models.cuenta, as: "cuenta", attributes: ["usuario"] },
                { model: models.rol, as: "rol", attributes: ["nombre"] }
            ]
        });
        if (lista === null) {
            lista = {};
        }
        res.status(200);
        res.json({ msg: "OK!", code: 200, info: lista });
    }

    async guardar(req, res) {

        if (req.body.hasOwnProperty('nombres') &&
            req.body.hasOwnProperty('apellidos') &&
            req.body.hasOwnProperty('direccion') &&
            req.body.hasOwnProperty('cedula') &&
            req.body.hasOwnProperty('rol')) {

            var uuid = require('uuid');
            var rol_aux = await rol.findOne({ where: { external_id: req.body.rol } });

            if (rol_aux != undefined) {

                var claveHash = function (clave) {
                    return bcrypt.hashSync(clave, bcrypt.genSaltSync(saltRounds), null);
                };

                var data = {
                    nombres: req.body.nombres,
                    apellidos: req.body.apellidos,
                    direccion: req.body.direccion,
                    cedula: req.body.cedula,
                    id_rol: rol_aux.id,
                    external_id: uuid.v4(),
                }

                if (rol_aux.nombre !== "Cliente") {
                    data.cuenta = {
                        usuario: req.body.usuario,
                        clave: claveHash(req.body.clave),
                    };
                }

                let transaction = await models.sequelize.transaction();

                try {
                    //se guarda primero la fuerte y luego la debil
                    var result = await persona.create(data, { include: [{ model: models.cuenta, as: "cuenta" }], transaction });
                    await transaction.commit();
                    if (result === null) {
                        res.status(401);
                        res.json({ msg: "Error", tag: "no se puede crear", code: 401 });
                    } else {
                        rol_aux.external_id = uuid.v4();
                        await rol_aux.save();
                        res.status(200);
                        res.json({ msg: "OK", code: 200 });
                    }

                } catch (error) {
                    if (transaction) await transaction.rollback();
                    res.status(203);
                    res.json({ msg: "Error", code: 203, error_msg: error.errors[0].message });
                }


            } else {
                res.status(400);
                res.json({ msg: "Error", tag: "El rol ingresado no existe", code: 400 });
            }

        } else {
            res.status(400);
            res.json({ msg: "Error", tag: "faltan datos", code: 400 });
        }

    }

    async modificar(req, res) {
        var person = await persona.findOne({ where: { external_id: req.body.external } })

        if (person === null) {
            res.status(400);
            res.json({ msg: "Error", tag: "El dato a modificar no existe", code: 400 });
        } else {

            if (req.body.hasOwnProperty('nombres') &&
                req.body.hasOwnProperty('apellidos') &&
                req.body.hasOwnProperty('direccion') &&
                req.body.hasOwnProperty('cedula')) {

                var uuid = require('uuid')

                person.nombres = req.body.nombres;
                person.apellidos = req.body.apellidos;
                person.direccion = req.body.direccion;
                person.cedula = req.body.cedula;
                person.external_id = uuid.v4();

                var result = await person.save();

                if (result === null) {
                    res.status(400);
                    res.json({ msg: "Error", tag: "No se han modificado los datos", code: 400 });
                } else {
                    res.status(200);
                    res.json({ msg: "Success", tag: "Datos modificados correctamente", code: 200 });
                }


            } else {
                res.status(400);
                res.json({ msg: "Error", tag: "faltan datos", code: 400 });
            }
        }
    }
}


module.exports = PersonaControl;

// modificar datos persona
