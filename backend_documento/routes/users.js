var express = require('express');
var router = express.Router();
let jwt = require('jsonwebtoken');
const multer = require('multer');
const bcrypt = require('bcrypt');


const personaC = require('../app/controls/PersonaControl');
let personaControl = new personaC();
const rolC = require('../app/controls/RolControl');
let rolControl = new rolC();
const cuentaC = require('../app/controls/CuentaControl');
let cuentaControl = new cuentaC();
const documentoC = require('../app/controls/DocumentoControl');
let documentoControl = new documentoC();
const ventaC = require('../app/controls/VentaControl');
let ventaControl = new ventaC();


//Middleware ----- filtro para peticiones //autentificacion
var auth = function middleware(req, res, next) {
  const token = req.headers['x-api-token'];
  if (token) {
    require("dotenv").config();
    const llave = process.env.KEY_PRI;
    jwt.verify(token, llave, async (err, decoded) => {
      if (err) {
        res.status(401);
        res.json({ msg: "token expirado o no valido", code: 401 });
      } else {
        var models = require('../app/models');
        req.decoded = decoded;
        let aux = await models.cuenta.findOne({ where: { external_id: req.decoded.external } });
        if (!aux) {
          res.status(401);
          res.json({ msg: "token no valido", code: 401 });
        } else {
          next();
        }
      }
    });
  } else {
    res.status(401);
    res.json({ msg: "No existe token", code: 401 });
  }
}

// Middleware ------- filtro para peticiones// autorizacion
var auth2 = function checkRole(rol) {
  return function (req, res, next) {
    const user = req.headers['rol-user'];
      if (rol.includes(user)) {
          next();
      } else {
          res.status(403)
          res.json({ msg: "Error", tag: 'Acceso no autorizado', code: 403 });
      }
  };
};


/* GET users listing. */
router.get('/', function (req, res, next) {
  res.json({ "version": "1.0", "name": "Libros" });
});

//Filtro para extenciones
const fileFilter = (req, file, cb) => {

  const extensiones = ['jpg', 'png', "jpeg", "mp3", "ogg"]

  // Verifica la extensión del archivo
  const isValidExtension = extensiones.includes(file.originalname.split('.').pop());

  if (isValidExtension) {
    cb(null, true);
  } else {
    cb(new Error('Archivo no permitido'));
  }
};

//guardar multiples archivos
const Storage = multer.diskStorage({
  destination: function (req, file, cb) {
    if (file.fieldname === 'img') {
      cb(null, 'public/images');
    } else if (file.fieldname === 'audio') {
      cb(null, 'public/multimedia');
    } else {
      cb(new Error('Campo no reconocido'));
    }
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname);
  }
});

const upload = multer({
  storage: Storage,
  fileFilter: fileFilter,
 // limits: { fileSize: 5 * 1024 * 1024 }
});

//api de documentos
router.post('/admin/documentos/guardar', auth, auth2(["Gerente"]), upload.fields([{ name: 'img', maxCount: 3 }, { name: 'audio', maxCount: 1 }]), documentoControl.guardar);
router.post('/admin/documentos/modificar/:external', auth, auth2(["Gerente"]), upload.fields([{ name: 'audio', maxCount: 1 }]), documentoControl.modificar);
router.get('/admin/documentos', auth, auth2(["Gerente"]), documentoControl.listar);
router.get('/admin/documentos/buscar/:external', auth, auth2(["Gerente"]), documentoControl.obtener);
router.get('/admin/documentos/novendidos', auth, auth2(["Gerente", "Vendedor"]), documentoControl.listarNoVendidos);
router.get('/admin/documentos/vendidos', auth, auth2(["Gerente", "Vendedor"]), documentoControl.listarVendidos);

//api de ventas
router.post('/admin/ventas/guardar', auth, upload.none(), auth, auth2(["Gerente", "Vendedor"]), ventaControl.guardar);
router.get('/admin/ventas', auth,auth2(["Gerente", "Vendedor"]), ventaControl.listar);
router.get('/admin/ventas/buscar/:vendedor', auth, auth2(["Gerente"]),ventaControl.buscarVenta);

//api de cuenta
router.post('/login', cuentaControl.inicioSesion);

//api de rol
router.get('/admin/rol', rolControl.listar);
router.post('/admin/rol/guardar', rolControl.guardar);

//api de personas
router.get('/admin/personas', auth, auth2(["Gerente", "Vendedor"]), personaControl.listar);
router.get('/admin/personas/vendedores', auth, auth2(["Gerente"]), personaControl.listarVendedor);
router.post('/admin/personas/guardar', auth, auth2(["Gerente"]), personaControl.guardar);
router.get('/admin/personas/buscar/:external', auth, auth2(["Gerente", "Vendedor"]), personaControl.obtener);
router.post('/admin/personas/modificar/:external', auth, auth2(["Gerente"]), personaControl.modificar);

module.exports = router;
