"use client";
import * as Yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup'
import { useForm } from 'react-hook-form';
import { estaSesion } from '@/hooks/SessionUtilClient';
import { inicio_sesion } from '@/hooks/Autenticacion';
import mensajes from '@/componentes/Mensajes';
import { useRouter } from 'next/navigation';
import Cookies from 'js-cookie';

export default function Inicio_sesion() {
    //router
    const router = useRouter();

    //validaciones
    const validationShema = Yup.object().shape({
        usuario: Yup.string().required('Ingrese su usuario'),
        clave: Yup.string().required('ingrese su clave')
    });

    const formOptions = { resolver: yupResolver(validationShema) };
    const { register, handleSubmit, formState } = useForm(formOptions);
    const { errors } = formState;

    const sendData = (data) => {
        var data = { "usuario": data.usuario, "clave": data.clave };
        
        inicio_sesion(data).then((info) => {
           // console.log("sesion" + estaSesion());
            if (!estaSesion()) {
                mensajes("Error en inicio de sesion", info.tag, "error")
            } else {
                //console.log(info);
                mensajes("Has Ingresado al Sistema", "Bienvenido Usuario", "success")
                Cookies.set("token", true);
                router.push("/principal");
            }
        })
    }

    return (

        <div className="container">
            <div className="container px-4 py-4 px-md-4 text-center text-lg-start my-4">
                <div className="row gx-lg-5 align-items-center mb-5">
                    <div className="col-lg-6 mb-5 mb-lg-0" >
                        <h1 className="my-5 display-5 fw-bold ls-tight" style={{ color: 'hsl(218, 81%, 85%)' }}>
                            Desarrollo Plataformas <br />
                            <span style={{ color: 'hsl(218, 81%, 75%)' }}>Practica 3</span>
                        </h1>
                    </div>

                    <div className="col-lg-6 mb-5 mb-lg-0 position-relative">
                        <div id="radius-shape-1" className="position-absolute rounded-circle shadow-5-strong"></div>
                        <div id="radius-shape-2" className="position-absolute shadow-5-strong"></div>

                        <div className="card bg-glass">
                            <div className="card-body px-4 py-5 px-md-5">
                                <form onSubmit={handleSubmit(sendData)}>


                                    <div className="form-outline mb-4">
                                        <input {...register('usuario')} name="usuario" id="usuario" className={`form-control ${errors.usuario ? 'is-invalid' : ''}`} />
                                        <label className="form-label" style={{ color: 'grey' }}>Usuario</label>
                                        <div className='alert alert-danger invalid-feedback'>{errors.usuario?.message}</div>
                                    </div>


                                    <div className="form-outline mb-4">
                                        <input {...register('clave')} type='password' name="clave" id="clave" className={`form-control ${errors.clave ? 'is-invalid' : ''}`} />
                                        <label className="form-label" style={{ color: 'grey' }} >Clave</label>
                                        <div className='alert alert-danger invalid-feedback'>{errors.clave?.message}</div>
                                    </div>


                                    <button type="submit" className="btn btn-primary btn-block mb-4">
                                        Acceder
                                    </button>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    )
}
