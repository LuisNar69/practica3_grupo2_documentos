'use client';
import mensajes from "@/componentes/Mensajes";
import { cambiarEstado, obtenerClientes, obtenerDoc, obtenerVentas } from "@/hooks/Conexion";
import { borrarSesion, getId, getR, getToken } from "@/hooks/SessionUtilClient";
import Link from "next/link";
import { useState } from "react";
import Menu from "@/componentes/Menu";
import Cookies from "js-cookie";
import { useRouter } from "next/navigation";

export default function Page() {
    let r = getR();
    let key = getToken();
    const router = useRouter();

    const [ventas, setVentas] = useState([]);
    const [vendedores, setVendedores] = useState([]);
    const [llamada, setLlamada] = useState(false);
    const [llamada2, setLlamada2] = useState(false);
    const [vendedor, setVendedor] = useState('');


    //llamar ventas
    if (!llamada) {
        obtenerVentas('admin/ventas', key, r).then((info) => {

            if (info.code === 200) {

                setVentas(info.info);
                setLlamada(true);
            } else if (info.code !== 200 && info.msg === "token expirado o no valido") {
                mensajes(info.msg, "Error", "error");
                Cookies.remove("token")
                router.push("/inicio_sesion")
            } else if (info.code !== 200 && info.tag === "Acceso no autorizado") {
                router.push("/principal")
                mensajes(info.tag, "Informacion", "error");
            } else {
                mensajes("No se pudo Listar las ventas", "Error", "error");
            }
        });
    };

    //llamar clientes
    if (!llamada2 && r === "Gerente") {
        obtenerClientes('admin/personas/vendedores', key, r).then((info) => {

            if (info.code === 200) {
                setVendedores(info.datos);
                setLlamada2(true);
            } else if (info.code !== 200 && info.tag === "token expirado o no valido") {
                mensajes(info.msg, "Error", "error");
                Cookies.remove("token")
                router.push("/inicio_sesion")
            } else if (info.code !== 200 && info.tag === "Acceso no autorizado") {
                router.push("/principal")
                mensajes(info.tag, "Informacion", "error");
            } else {
                mensajes("No se pudo Listar las ventas", "Error", "error");
            }
        });
    };

    const handleSelectChange = (e) => {
        setVendedor(e.target.value);
    };

    const handleBuscarClick = () => {
        obtenerVentas('admin/ventas/buscar/' + vendedor, key, r).then((info) => {
            if (info.code === 200) {

                setVentas(info.info);
                setLlamada(true);
            } else if (info.code !== 200 && info.tag === "token expirado o no valido") {
                mensajes(info.msg, "Error", "error");
                Cookies.remove("token")
                router.push("/inicio_sesion")
            } else if (info.code !== 200 && info.tag === "Acceso no autorizado") {
                router.push("/principal")
                mensajes(info.tag, "Informacion", "error");
            } else {
                mensajes("No se pudo Listar los documentos", "Error", "error");
            }
        });
    };


    return (

        <div className="row">
            <Menu></Menu>
            <h1 style={{ textAlign: "center" }}>LISTAR VENTAS</h1>
            <div className="container-fluid">
                {r === "Gerente" && (
                    <div className="input-group mb-3">
                        <select className="form-control" onChange={handleSelectChange} aria-describedby="button-addon2">
                            <option value="">Elija un vendedor</option>
                            {vendedores.map((aux, i) => (
                                <option key={i} value={aux.id}>
                                    {`${aux.nombres} ${aux.apellidos}`}
                                </option>
                            ))}
                        </select>
                        <button className="btn btn-outline-secondary" type="button" id="button-addon2" onClick={handleBuscarClick}>Buscar</button>
                    </div>
                )}

                <div className="col-12 mb-4" style={{ alignItems: "center" }}>
                    <Link style={{ margin: "15px" }} href="/ventas/nuevo" className="btn btn-success font-weight-bold">Registrar</Link>
                </div>
                <table className="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Nro</th>
                            <th>Fecha</th>
                            <th>Metodo_Pago</th>
                            <th>Estado</th>
                            <th>Total</th>
                            <th>Subtotal</th>
                            <th>Descuento</th>
                            <th>Cantidad</th>
                        </tr>
                    </thead>
                    <tbody>
                        {ventas && ventas.length > 0 ? (
                            ventas.map((dato, i) => (
                                <tr key={i}>
                                    <td>{dato.numero}</td>
                                    <td>{dato.fecha}</td>
                                    <td>{dato.metodo_pago}</td>
                                    <td>{dato.estado}</td>
                                    <td>{dato.total}</td>
                                    <td>{dato.detalleVenta[0].subtotal}</td>
                                    <td>{dato.detalleVenta[0].descuento}</td>
                                    <td>{dato.detalleVenta[0].cantidad}</td>

                                </tr>
                            ))
                        ) : (
                            <tr>
                                <td colSpan="4">No hay datos disponibles</td>
                            </tr>
                        )}
                    </tbody>
                </table>

            </div>
        </div>
    );
}


