'use client';
import mensajes from "@/componentes/Mensajes";
import * as Yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup'
import { useForm } from 'react-hook-form';
import Link from "next/link";
import { useRouter } from 'next/navigation';
import { enviar, enviarVenta, obtenerClientes, obtenerDoc, obtenerMaterias } from "@/hooks/Conexion";
import { useState } from "react";
import { getE, getId, getR, getToken } from "@/hooks/SessionUtilClient";
import Menu from "@/componentes/Menu";
import Cookies from "js-cookie";

export default function Page() {

  const router = useRouter();
  let r = getR();
  const key = getToken();
  const user = getId();
  const external = getE();


  const [llamada, setLlamada] = useState(false);
  const [llamada2, setLlamada2] = useState(false);
  const [clientes, setClientes] = useState([]);
  const [cedula, setCedula] = useState("");
  const fechaActual = new Date();
  const fechaFormateada = fechaActual.toISOString().split('T')[0];
  const [nro, setNro] = useState(0);
  const [data, setData] = useState([]);
  const [lldoc, setLldoc] = useState(false);
  const [mostrarTabla, setMostrarTabla] = useState(false);
  const [selectedRows, setSelectedRows] = useState([]);
  const [dataDes, setDataDes] = useState([]);


  //llamar clientes
  if (!llamada) {
    obtenerClientes('admin/personas', key, r).then((info) => {
      if (info.code === 200) {

        setClientes(info.datos);
        setLlamada(true);
      } else if (info.code !== 200 || info.msg === "token expirado o no valido") {
        mensajes(info.msg, "Error", "error");
        Cookies.remove("token")
        router.push("/inicio_sesion")
      } else if (info.code !== 200 && info.tag === "Acceso no autorizado") {
        router.push("/principal")
        mensajes(info.tag, "Informacion", "error");
      } else {
        mensajes("No se pudo Listar los clientes", "Error", "error");
      }
    });
  };

  //llamar clientes
  if (!llamada2) {
    obtenerDoc('admin/ventas', key, r).then((info) => {
      if (info.code === 200) {

        setNro(info.info.length);
        setLlamada2(true);
      } else if (info.code !== 200 || info.msg === "token expirado o no valido") {
        mensajes(info.msg, "Error", "error");
        Cookies.remove("token")
        router.push("/inicio_sesion")
      } else {
        mensajes("No se pudo Listar los clientes", "Error", "error");
      }
    });
  };
  //validaciones
  const validationShema = Yup.object().shape({
    // metodo: Yup.string().required('Seleccione un metodo'),
    external_cliente: Yup.string().required('Seleccione un cliente'),
  });

  const formOptions = { resolver: yupResolver(validationShema) };
  const { register, handleSubmit, formState } = useForm(formOptions);
  const { errors } = formState;

  if (!lldoc) {
    obtenerDoc('admin/documentos/novendidos', key, r).then((info) => {
      if (info.code === 200) {
        setData(info.info);
        setLldoc(true);
      } else if (info.code !== 200 || info.msg === "token expirado o no valido") {
        mensajes(info.msg, "Error", "error");
        Cookies.remove("token")
        router.push("/inicio_sesion")
      } else if (info.code !== 200 && info.tag === "Acceso no autorizado") {
        router.push("/principal")
        mensajes(info.tag, "Informacion", "error");
      } else {
        mensajes("No se pudo Listar los documentos", "Error", "error");
      }
    });
  }

  const subtotal = () => {
    var subtotal = 0;

    dataDes.forEach((element) => {
      subtotal += parseFloat(element.precio);
    });
    return subtotal;
  };

  const total = () => {
    var total = 0;
    var iva = 0.12;
    iva = 0.12 * subtotal();
    total = parseFloat(iva) + subtotal();
    return total;
  };

  const handleRowSelection = (index) => {
    const isSelected = selectedRows.includes(index);

    if (isSelected) {
      // Si la fila ya está seleccionada, qse quita de la lista
      setSelectedRows(selectedRows.filter((rowIndex) => rowIndex !== index));
    } else {
      // Si la fila no está seleccionada, agrégala a la lista de seleccionada
      setSelectedRows([...selectedRows, index]);
    }
  };

  const handleAgregarLibro = () => {
    setSelectedRows([])
    setMostrarTabla(true);
  };

  const handleCerrarTabla = () => {
    setMostrarTabla(false);
    // setSelectedItems([]);
  };

  const descripcionAux = () => {
    const newDataDes = selectedRows.map((index) => data[index]);
    setDataDes((prevDataDes) => [...prevDataDes, ...newDataDes]);
    setSelectedRows([]); // Limpiar las filas seleccionadas después de guardarlas

    // Filtrar elementos seleccionados de data para obtener "libros disponibles"
    const librosDisponibles = data.filter((libro, i) => !selectedRows.includes(i));
    setData(librosDisponibles);
  };

  const sendData = (data) => {

    if (data.estado !== "CANCELADA") {
      const numero = "000000" + nro.toString();
      const valuesubtotal = subtotal();
      const valuetotal = total();
      const valuecantidad = dataDes.length;
      var datos = {
        'numero': numero,
        'metodo': data.metodo,
        'estado': data.estado,
        'total': valuetotal,
        'subtotal': valuesubtotal,
        'descuento': 0,
        'external_vendedor': external,
        "external_cliente": data.external_cliente,
        'cantidad': valuecantidad,
      };

      enviarVenta('admin/ventas/guardar', datos, dataDes, key, r).then((info) => {
        console.log(info)
        if (info.code !== 200) {
          if (info.msg === 'token expirado o no valido') {
            mensajes(info.msg, "Error", "error");
            Cookies.remove("token")
            router.push("/inicio_sesion")
          } else {
            mensajes("Venta no se pudo guardar", "Error", "error")
          }
        } else {
          mensajes("Venta guardada correctamente", "Informacion", "success")
          router.push("/ventas");
        }
      });
    } else {
      mensajes("Venta Cancelada", "Informacion", "")
      router.push("/ventas");

    }
  }


  return (
    <div className='wrapper'>
      <Menu />
      <div className="card text-center" style={{ margin: 10 }}>
        <div className="card-body">
          <div className='container-fluid'>

            <form className='user' onSubmit={handleSubmit(sendData)} >

              <div className="row mn-4">
                <div className="col">
                  <div className="form-group row">
                    <label className="col-lg-3 col-form-label">Numero:</label>
                    <div className="col-lg-3">
                      <input name="numero" id="numero" className="form-control" {...register('numero')} onChange={() => { }} value={`000000${nro}`} />
                    </div>
                  </div>
                </div>
                <br />
                <div className="col">
                  <div className="form-group row">
                    <label className="col-lg-3 col-form-label" >Fecha:</label>
                    <div className="col-lg-3">
                      <input name="fecha" id="fecha" className="form-control" disabled defaultValue={fechaFormateada} />
                    </div>
                  </div>
                </div>
              </div>

              <br />

              <div className="row mb-4">
                <div className="col">
                  <div className="form-group row">
                    <label className="col-lg-3 col-form-label" >Cliente:</label>
                    <div className="col-lg-3">
                      <select className='form-control' style={{ height: 35, width: 400 }} name="external_cliente" id="external_cliente" {...register('external_cliente')} onChange={(e) => setCedula(e.target.options[e.target.selectedIndex].getAttribute('ced'))}>

                        <option value="">Elija un cliente</option>
                        {clientes.map((aux, i) => (
                          <option key={i} value={aux.id} ced={aux.cedula}>
                            {`${aux.nombres} ${aux.apellidos}`}
                          </option>
                        ))}
                      </select>
                    </div>
                  </div>
                </div>
                <div className="col">
                  <div className="form-group row">
                    <label className="col-lg-3 col-form-label">Cedula:</label>
                    <div className="col-lg-3">
                      <input type="text" style={{ height: 35, width: 400 }} disabled className="form-control" name="cedula" id="cedula" defaultValue={cedula} />
                    </div>
                  </div>
                </div>
              </div>


              <div className="row mb-4">
                <div className="col">
                  <div className="form-group row">
                    <label className="col-lg-3 col-form-label">Vendedor:</label>
                    <div className="col-lg-3">
                      <input type="text" style={{ height: 35, width: 400 }} disabled className="form-control" name="vendedor" id="vendedor" defaultValue={user} />
                    </div>
                  </div>
                </div>
                <div className="col">
                  <div className="form-group row">

                    <div className="col-lg-3">

                    </div>
                  </div>
                </div>
              </div>

              <div className="row mb-4">
                <div className="col">
                  <div className="form-group row">
                    <label className="col-lg-3 col-form-label">Metodo pago:</label>
                    <div className="col-lg-3">
                      <select className='form-control' {...register('metodo')} name="metodo" id="metodo" style={{ height: 35, width: 400 }} >
                        <option>TARJETA_DEBITO</option>
                        <option>TARJETA_CREDITO</option>
                        <option>EFECTIVO</option>

                      </select>

                    </div>
                  </div>
                </div>
                <div className="col">
                  <div className="form-group row">
                    <label className="col-lg-3 col-form-label">Estado Venta:</label>
                    <div className="col-lg-3">
                      <select className='form-control' {...register('estado')} name="estado" id="estado" style={{ height: 35, width: 400 }}  >
                        <option>PAGADA</option>
                        <option>CANCELADA</option>

                      </select>

                    </div>
                  </div>
                </div>
              </div>

              <div className="row mt-4">
                <div className="col-md">
                  <button type="button" onClick={handleAgregarLibro} className="btn btn-success" style={{ margin: 10 }}>Agregar Documento</button>

                  {mostrarTabla && (
                    <div className="card text-center" style={{ margin: 20 }}>
                      <div className="card-header" style={{ color: "black" }}>   DOCUMENTOS DISPONIBLES    </div>
                      <div className="card-body">
                        <div className='container-fluid'>
                          <div style={{ maxHeight: '200px', overflowY: 'auto' }}>
                            <table className="table table-bordered table-hover" >
                              <thead>
                                <tr>
                                  <th>Titulo</th>
                                  <th>Autor</th>
                                  <th>Tipo</th>
                                  <th>Precio</th>
                                </tr>
                              </thead>
                              <tbody>
                                {data && data.length > 0 ? (
                                  data.map((dato, i) => (
                                    <tr
                                      key={i}
                                      style={{
                                        backgroundColor: selectedRows.includes(i) ? '#f0f8ff' : 'transparent',
                                        color: selectedRows.includes(i) ? '#333' : '#000',

                                      }}
                                      onClick={() => handleRowSelection(i)}
                                    >
                                      <td>{dato.titulo}</td>
                                      <td>{dato.autor}</td>
                                      <td>{dato.tipo}</td>
                                      <td>{dato.precio}</td>
                                    </tr>
                                  ))
                                ) : (
                                  <tr>
                                    <td colSpan="4">No hay datos disponibles</td>
                                  </tr>
                                )}
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                      <button className='btn btn' onClick={() => { handleCerrarTabla(); descripcionAux(); }}>Agregar</button>
                    </div>
                  )}

                  <table className="table table-striped">
                    <thead>
                      <tr>
                        <th>Descripción</th>
                        <th className="text-right">Precio Unitario</th>

                      </tr>
                    </thead>
                    <tbody>
                      {dataDes.map((element, index) => {
                        return (
                          <tr key={index}>
                            <td>{"Libro: " + element.titulo + ",  autor: " + element.autor + ",  tipo: " + element.tipo}</td>
                            <td>{element.precio}</td>
                          </tr>
                        );
                      })}
                    </tbody>
                  </table>

                </div>
              </div>

              <div className="row mt-4">
                <div className="col-md">

                </div>
              </div>
              <div className="row mb-4">
                <div className="col">
                  <div className="form-group row">
                    <label className="col-lg-3 col-form-label">Total:</label>
                    <div className="col-lg-3">
                      <input type="text" style={{ height: 35, width: 400 }} name="total" id="total" className="form-control" onChange={() => { }} value={total()} />

                    </div>
                  </div>
                </div>
                <div className="col">
                  <div className="form-group row">
                    <label className="col-lg-3 col-form-label">Subtotal:</label>
                    <div className="col-lg-3">
                      <input type="text" style={{ height: 35, width: 400 }} name="subtotal" id="subtotal" className="form-control" onChange={() => { }} value={subtotal()} />

                    </div>
                  </div>
                </div>

              </div>
              <div className="col">
                <div className="form-group row">
                  <label className="col-lg-3 col-form-label">Cantidad:</label>
                  <div className="col-lg-3">
                    <input type="text" style={{ height: 35, width: 400 }} className="form-control" name="cantidad" id="cantidad" onChange={() => { }} value={dataDes.length} />

                  </div>
                </div>
              </div>
              <br />
              <button type='submit' className="btn btn-success">Guardar Venta</button>
            </form>
          </div>
        </div>
      </div >

    </div >
  );
};



