'use client';
import mensajes from "@/componentes/Mensajes";
import * as Yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup'
import { useForm, useWatch } from 'react-hook-form';
import Link from "next/link";
import { useRouter } from 'next/navigation';
import { Documento, enviar, enviardoc } from "@/hooks/Conexion";
import { useState } from "react";
import { getR, getToken } from "@/hooks/SessionUtilClient";
import Menu from '@/componentes/Menu';

export default function Page() {

  const router = useRouter();

  let r = getR();
  let key = getToken();

  const [selectedImages, setSelectedImages] = useState([]);
  const [estado, setEstado] = useState(false);
  const [tipo, setTipo] = useState('');
  const [llamada, setLlamada] = useState(false);

  //buscar documento
  if (!llamada) {
    Documento("/admin/documentos", key,r ).then((info) => {
      if (info.code === 200) {
        //console.log(info.info)
        const documentoData = info.info;
       // setDocumento(documentoData);
        setLlamada(true);
      } else if (info.code !== 200 && info.tag === "Acceso no autorizado") {
        router.push("/principal")
        mensajes(info.tag, "Informacion", "error");
      } else {
        mensajes("No se pudo Listar los documentos", "Error", "error");
      }
    });
  };


  //guardar imagenes en un array
  const handleImageChange = (event) => {
    const files = event.target.files;

    const newImagesArray = [...selectedImages, ...Array.from(files)];

    if (newImagesArray.length <= 3) {
      setSelectedImages(newImagesArray);
    } else {
      mensajes("Solo se permiten hasta 3 imágenes", 'error', 'Error');
    }
  };

  //mostrar  u ocultar campo de archivo  audio 
  const handleTipoChange = (event) => {
    const selectedTipo = event.target.value;
    setTipo(selectedTipo);
    // Dependiendo del tipo seleccionado, actualiza el estado
    if (selectedTipo === 'TEXTO') {
      setEstado(false);
    } else if (selectedTipo === 'AUDIO') {
      setEstado(true);
    }
  };

  //validaciones
  const validationShema = Yup.object().shape({
    titulo: Yup.string().required('Ingrese un titulo'),
    autor: Yup.string().required('ingrese un autor'),
    precio: Yup.number().required('Ingrese un precio'),
    isbn: Yup.string().required('Ingrese isbn'),
    tipo: Yup.string().required('Seleccione un tipo'),
    img: Yup.mixed().required('Seleccione por lo menos una imagen'),
  });

  const formOptions = { resolver: yupResolver(validationShema) };
  const { register, handleSubmit, formState } = useForm(formOptions);
  const { errors } = formState;

  //Metodo para guardar documento
  const sendData = (data) => {
    var datos = {
      'titulo': data.titulo,
      'autor': data.autor,
      'precio': data.precio,
      'isbn': data.isbn,
      'tipo': tipo,
    };

    let key = getToken();

    enviardoc('admin/documentos/guardar', selectedImages, data.audio, datos, key, r).then((info) => {
      if (info.code !== 200) {
        if (info.msg === 'token expirado o no valido') {
          bmensajes(info.msg, "Error", "error");
          Cookies.remove("token")
          router.push("/inicio_sesion")
        } else {
          mensajes("Documento no se pudo guardar", "Error", "error")
        }
      } else {
        mensajes("Documento guardado correctamente", "Informacion", "success")
        router.push("/documentos");
      }
    });
  };


  return (
    <div className="wrapper" >
      <Menu />
      <center>
        <br /><br />
        <div className="d-flex flex-column" style={{ width: 700 }}>
          <h5 className="title" style={{ color: "black", font: "bold" }}>REGISTRO DOCUMENTOS</h5>
          <br />

          <div className='container-fluid'>
            <form className="user" onSubmit={handleSubmit(sendData)}>
              <div className="row mb-4">
                <div className="col">
                  <input {...register('titulo')} name="titulo" id="titulo" placeholder="Ingrese el titulo" className={`form-control ${errors.titulo ? 'is-invalid' : ''}`} />
                  <div className='alert alert-danger invalid-feedback'>{errors.titulo?.message}</div>
                </div>
                <div className="col">
                  <input {...register('autor')} name="autor" id="autor" placeholder="Ingrese el autor" className={`form-control ${errors.autor ? 'is-invalid' : ''}`} />
                  <div className='alert alert-danger invalid-feedback'>{errors.autor?.message}</div>
                </div>
              </div>
              <div className="row mb-4">
                <div className="col">
                  <input {...register('precio')} name="precio" id="precio" placeholder="Ingrese el precio" className={`form-control ${errors.precio ? 'is-invalid' : ''}`} />
                  <div className='alert alert-danger invalid-feedback'>{errors.precio?.message}</div>
                </div>
                <div className="col">
                  <input {...register('isbn')} name="isbn" id="isbn" placeholder="Ingrese el isbn" className={`form-control ${errors.isbn ? 'is-invalid' : ''}`} />
                  <div className='alert alert-danger invalid-feedback'>{errors.isbn?.message}</div>
                </div>
              </div>
              <div className="row mb-4">
                <div className="col">
                  <select {...register('tipo')} name="tipo" id="tipo" onChange={handleTipoChange} className={`form-control ${errors.tipo ? 'is-invalid' : ''}`}>
                    <option>Elija un Tipo</option>
                    <option>TEXTO</option>
                    <option>AUDIO</option>
                  </select>
                  <div className='alert alert-danger invalid-feedback'>{errors.tipo?.message}</div>
                </div>
                <div className="col">

                </div>
              </div>

              <div className="row mb-4">
                <div className="col">
                  {estado && (
                    <div>
                      <label className="custom-file-label" style={{ color: "black", fontWeight: "bold", fontSize: '16px' }}>File audio</label>
                      <input className="custom-file-input form-control form-control-user"
                        {...register('audio')} type="file" accept="audio/mp3, audio/ogg" />
                    </div>
                  )}
                </div>
              </div>

              <div className="row mb-4">
                <div className="col">
                  <br />
                  <label className="custom-file-label" style={{ color: "black", fontWeight: "bold", fontSize: '16px' }}>Galeria imagenes</label>
                  <input {...register('img')} type="file" accept="image/jpeg, image/png" className={`form-control ${errors.tipo ? 'is-invalid' : ''}`} multiple onChange={handleImageChange} />
                  <div className='alert alert-danger invalid-feedback'>{errors.img?.message}</div>
                </div>

              </div>

              <div>
                <br />
                <h3 style={{ color: "black", fontWeight: "bold", fontSize: '16px' }}>Imagenes seleccionadas:</h3>
                <ul>
                  {selectedImages.map((image, index) => (
                    <li style={{ color: "black", font: "bold", fontSize: '13px' }} key={index}>{image.name}</li>
                  ))}
                </ul>
              </div>


              <hr />
              <button type='submit' className="btn btn-success">GUARDAR</button>

            </form>
          </div>
          {<Link style={{ margin: "5px" }} href="/documentos" className="btn btn-danger font-weight-bold">Regresar</Link>}
        </div>
      </center>
    </div>
  );
}

