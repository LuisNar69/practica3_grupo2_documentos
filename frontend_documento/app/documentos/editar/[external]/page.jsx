'use client';
import mensajes from "@/componentes/Mensajes";
import * as Yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup'
import { useForm } from 'react-hook-form';
import Link from "next/link";
import { useRouter } from 'next/navigation';
import { Documento, modoc } from "@/hooks/Conexion";
import { useEffect, useState } from "react";
import { getR, getToken } from "@/hooks/SessionUtilClient";
import Menu from "@/componentes/Menu";

export default function Page({ params }) {
  const { external } = params;
  let key = getToken();
  let r = getR();

  const router = useRouter();

  const [documento, setDocumento] = useState([]);
  const [llamada, setLlamada] = useState(false);
  const [tipo, setTipo] = useState('');

  //buscar documento
  if (!llamada) {
    Documento("/admin/documentos/buscar/" + external, key, r).then((info) => {
      if (info.code === 200) {
 
        const documentoData = info.info;
        setDocumento(documentoData);
        setLlamada(true);
      } else if (info.code !== 200 && info.tag === "Acceso no autorizado") {
        router.push("/principal")
        mensajes(info.tag, "Informacion", "error");
      } else {
        mensajes("No se pudo Listar los documentos", "Error", "error");
      }
    });
  };

    //mostrar  u ocultar campo de archivo  audio 
    const handleTipoChange = (event) => {
      const selectedTipo = event.target.value;
      setTipo(selectedTipo);
    };

  //validaciones
  const validationShema = Yup.object().shape({
    titulo: Yup.string().required('Ingrese un titulo'),
    autor: Yup.string().required('ingrese un autor'),
    precio: Yup.number().required('Ingrese un precio'),
    isbn: Yup.string().required('Ingrese isbn'),
   // tipo: Yup.string().required('Seleccione un tipo'),
  });

  const formOptions = { resolver: yupResolver(validationShema) };
  const { register, handleSubmit, setValue, formState } = useForm(formOptions);
  const { errors } = formState;

  //Metodo para guardar documento
  const sendData = (data) => {

    var datos = {
      'titulo': data.titulo,
      'autor': data.autor,
      'precio': data.precio,
      'isbn': data.isbn,
      'tipo': tipo,
    };

    let key = getToken();
    modoc('admin/documentos/modificar/' + external, data.audio, datos, key, r).then((info) => {
      if (info.code !== 200) {
        mensajes("Documento no se pudo modificar", "Error", "error")
      } else {
        mensajes("Documento modificado correctamente", "Informacion", "success")
        router.push("/documentos");
      }
    });
  };

  useEffect(() => {
    if (documento) {
      setValue('titulo', documento.titulo);
      setValue('autor', documento.autor);
      setValue('isbn', documento.isbn);
      setValue('precio', documento.precio);
      setValue('tipo', documento.tipo);
    //  setValue('audio', documento.archivo)
    if(tipo === '' || tipo === undefined || tipo === null){
      setTipo(documento.tipo);
    }
    }
  }, [documento, setValue]);


  return (
    <div className="wrapper" >
      <Menu />
      <center>
        <br /><br />
        <div className="d-flex flex-column" style={{ width: 700 }}>
          <h5 className="title" style={{ color: "black", font: "bold" }}>MODIFICAR DOCUMENTO</h5>
          <br />

          <div className='container-fluid'>
            <form className="user" onSubmit={handleSubmit(sendData)}>
              <div className="row mb-4">
                <div className="col">
                  <input {...register('titulo')} name="titulo" id="titulo" placeholder="Ingrese el titulo" className={`form-control ${errors.titulo ? 'is-invalid' : ''}`} />
                  <div className='alert alert-danger invalid-feedback'>{errors.titulo?.message}</div>
                </div>
                <div className="col">
                  <input {...register('autor')} name="autor" id="autor" placeholder="Ingrese el autor" className={`form-control ${errors.autor ? 'is-invalid' : ''}`} />
                  <div className='alert alert-danger invalid-feedback'>{errors.autor?.message}</div>
                </div>
              </div>
              <div className="row mb-4">
                <div className="col">
                  <input {...register('precio')} name="precio" id="precio" placeholder="Ingrese el precio" className={`form-control ${errors.precio ? 'is-invalid' : ''}`} />
                  <div className='alert alert-danger invalid-feedback'>{errors.precio?.message}</div>
                </div>
                <div className="col">
                  <input {...register('isbn')} name="isbn" id="isbn" placeholder="Ingrese el isbn" className={`form-control ${errors.isbn ? 'is-invalid' : ''}`} />
                  <div className='alert alert-danger invalid-feedback'>{errors.isbn?.message}</div>
                </div>
              </div>

              <div className="row mb-4">
                <div className="col">
                  <select {...register('tipo')} name="tipo" id="tipo" onChange={handleTipoChange} className="form-control">
                    <option>Elija un Tipo</option>
                    <option>TEXTO</option>
                    <option>AUDIO</option>
                  </select>
               
                </div>
                <div className="col">

                </div>
              </div>

              <div className="row mb-4">
                <div className="col">

                  <label className="custom-file-label" style={{ color: "black", fontWeight: "bold", fontSize: '16px' }}>File audio</label>
                  <input className="custom-file-input form-control form-control-user"
                    {...register('audio')} type="file" accept="audio/mp3, audio/ogg" />

                </div>
              </div>

              <hr />
              <button type='submit' className="btn btn-success">GUARDAR</button>

            </form>
          </div>
          {<Link style={{ margin: "5px" }} href="/documentos" className="btn btn-danger font-weight-bold">Regresar</Link>}
        </div>
      </center>
    </div>
  );
}