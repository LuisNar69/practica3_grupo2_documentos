'use client';
import mensajes from "@/componentes/Mensajes";
import { obtenerDoc } from "@/hooks/Conexion";
import { getR, getToken } from "@/hooks/SessionUtilClient";
import Cookies from 'js-cookie';
import Link from "next/link";
import { useRouter } from "next/navigation";
import { useState } from "react";
import Menu from '@/componentes/Menu';

export default function Page() {
    let r = getR();
    let key = getToken();
    const router = useRouter();
    const [documentos, setDocumentos] = useState([]);
    const [llamada, setLlamada] = useState(false);

    //llamar documentos
    if (!llamada) {
        obtenerDoc('admin/documentos', key, r).then((info) => {
           // console.log(info)
            if (info.code === 200) {
                setDocumentos(info.info);
                setLlamada(true);
            } else if (info.code !== 200 && info.tag === "token expirado o no valido") {
                mensajes(info.tag, "Error", "error");
                Cookies.remove("token")
                router.push("/inicio_sesion")
            } else if (info.code !== 200 && info.tag === "Acceso no autorizado") {
                router.push("/principal")
                mensajes(info.tag, "Informacion", "error");
            } else {
                mensajes("No se pudo Listar los documentos", "Error", "error");
            }
        });
    };

    return (

        <div className="row">
            <Menu />
            <h1 style={{ textAlign: "center" }}> Documentos</h1>
            <div className="container-fluid">
                <div className="col-12 mb-4" style={{ alignItems: "center" }}>

                    <Link style={{ margin: "15px" }} href="/documentos/nuevo" className="btn btn-success font-weight-bold">Registrar</Link>

                </div>
                <div style={{ maxHeight: '450px', overflowY: 'auto' }}>
                    <table className="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Nro</th>
                                <th>Titulo</th>
                                <th>Autor</th>
                                <th>Tipo</th>
                                <th>Vendido</th>
                                <th>Galeria</th>
                                <th>Audio</th>
                                <th>Precio</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            {documentos.map((dato, i) => (
                                <tr key={i}>
                                    <td>{i + 1}</td>
                                    <td>{dato.titulo}</td>
                                    <td>{dato.autor}</td>
                                    <td>{dato.tipo}</td>
                                    <td>{dato.estado.toString()}</td>
                                    <td>
                                        {dato.fotoDocumento.map((foto, index) => (
                                            <img style={{ width: 50, height: 50 }} key={index} src={foto.url} />
                                        ))}
                                    </td>
                                    <td>
                                        {dato.archivo && dato.archivo !== 'NONE' ? (
                                            <audio controls>
                                                <source src={dato.archivo} />
                                            </audio>
                                        ) : (
                                            <span>Sin archivo</span>
                                        )}
                                    </td>
                                    <td>{dato.precio}</td>
                                    <td>
                                        {<Link style={{ margin: "5px" }} href={`/documentos/editar/${dato.external_id}`} className="btn btn-warning font-weight-bold">Modificar</Link>}
                                    </td>
                                </tr>
                            )
                            )}
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    );
}


