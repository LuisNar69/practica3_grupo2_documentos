let URL = 'http://localhost:3006/api/';

//devolver la url
export function url_api() {
    return URL;
}

//enviar peticion
export async function enviar(recurso, data, key = "", rol) {
    let headers = {};

    if (key !== "") {
        headers = {
            'Accept': 'application/json',
            "Content-Type": "application/json",
            'x-api-token': key,
            'rol-user': rol
        };
    } else {
        headers = {
            'Accept': 'application/json',
            "Content-Type": "application/json",
            'rol-user': rol
        };
    }

    const response = await (fetch(URL + recurso, {
        method: "POST",
        headers: headers,
        body: JSON.stringify(data)
    }));

    return await response.json();
}

//enviar peticion
export async function enviardoc(recurso, imagen, audio, data, key = "", rol) {
    const formData = new FormData();

    for (let i = 0; i < imagen.length; i++) {
        formData.append('img', imagen[i]); // Agregar cada imagen al FormData
    }

    if (audio != undefined) {
        formData.append('audio', audio[0]);
    }
    formData.append('car', JSON.stringify(data));

    let headers = {};

    headers = {
        //'Accept': 'application/json',
        // "Content-Type": "application/json",
        'x-api-token': key,
        'rol-user': rol
    };

    const response = await (fetch(URL + recurso, {
        method: "POST",
        headers: headers,
        body: formData,
    }));

    return await response.json();
}

//documentos
export async function obtenerDoc(recurso, key = "", rol) {
    let headers = {};
    headers = { 
        'Accept': 'application/json',
        "Content-Type": "application/json",
        'x-api-token': key,
        'rol-user': rol
    };

    const response = await (await fetch(URL + recurso, {
        method: "GET",
        headers: headers,
        cache: 'no-store'
    })).json();

    // console.log("respuesta odcs: " + response)
    return response;
}

//documentos
export async function obtenerClientes(recurso, key = "", rol) {
    let headers = {};

    headers = {
        'Accept': 'application/json',
        "Content-Type": "application/json",
        'x-api-token': key,
        'rol-user': rol
    };

    const response = await (await fetch(URL + recurso, {
        method: "GET",
        headers: headers,
        cache: 'no-store'
    })).json();

    // console.log("respuesta odcs: " + response)
    return response;
}

//Ventas
export async function obtenerVentas(recurso, key = "", rol) {
    let headers = {};

    headers = {
        'Accept': 'application/json',
        "Content-Type": "application/json",
        'x-api-token': key,
        'rol-user': rol
    };

    const response = await (await fetch(URL + recurso, {
        method: "GET",
        headers: headers,
        cache: 'no-store'
    })).json();

    // console.log("respuesta odcs: " + response)
    return response;
}

//enviar peticion
export async function modoc(recurso, audio, data, key = "", rol) {
    const formData = new FormData();
    //console.log("audio",audio)
    if (audio === undefined) {
        formData.append('audio', null);
    } else {
        formData.append('audio', audio[0]);
    }

    formData.append('car', JSON.stringify(data));

    let headers = {};

    headers = {
        //'Accept': 'application/json',
        // "Content-Type": "application/json",
        'x-api-token': key,
        'rol-user': rol
    };

    const response = await (fetch(URL + recurso, {
        method: "POST",
        headers: headers,
        body: formData,
    }));

    return await response.json();
}

//Buscar documento
export async function Documento(recurso, key = "", rol) {
    let headers = {};

    headers = {
        'Accept': 'application/json',
        "Content-Type": "application/json",
        'x-api-token': key,
        'rol-user': rol
    };

    const response = await (await fetch(URL + recurso, {
        method: "GET",
        headers: headers,
        cache: 'no-store'
    })).json();


    return response;
}

export const enviarVenta = async (recurso,data, array, key="", rol) => {
    const formData = new FormData();
    array.forEach((element) => {
        formData.append('dataDes',JSON.stringify(element));
    });
    formData.append('factura', JSON.stringify(data));
   
    const cabeceras = {
        'x-api-token': key,
        'rol-user': rol
    };

    const datos = await (await fetch(URL + recurso, {
        method: "POST",
        headers: cabeceras,
        body: formData,
    })).json();
    return datos;
}




