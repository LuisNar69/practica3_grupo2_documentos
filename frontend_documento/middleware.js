import { NextResponse } from 'next/server'


export function middleware(request) {
    let tokenValue = false;
    const tokenObject = request.cookies.get('token');

    if (tokenObject) {
        tokenValue = JSON.parse(tokenObject.value);
    }

    if (!tokenValue && request.nextUrl.pathname.startsWith('/principal')) {
        return NextResponse.redirect(new URL("/inicio_sesion", request.url));
    }

    if (!tokenValue && request.nextUrl.pathname.startsWith('/documentos')) {
        return NextResponse.redirect(new URL("/inicio_sesion", request.url));
    }

    if (tokenValue && request.nextUrl.pathname.startsWith('/inicio_sesion')) {
        return NextResponse.redirect(new URL('/principal', request.url))
    }
}

// See "Matching Paths" below to learn more
export const config = {
    matcher: ['/documentos(.*)', '/inicio_sesion', '/principal'],
}