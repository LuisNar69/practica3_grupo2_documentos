'use client';
import { borrarSesion } from "@/hooks/SessionUtil";
import Link from "next/link";
import Cookies from 'js-cookie';
import { useEffect } from "react";
import { estaSesion } from "@/hooks/SessionUtilClient";

export default function Menu() {

    const salir = () => {
        Cookies.remove("token");
        borrarSesion();
    }

    return (
        <nav className="navbar navbar-expand-lg bg-body-tertiary" data-bs-theme="dark">
            <div className="container-fluid">
                <Link className="navbar-brand" href={''}>VENTA LIBROS</Link>

                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav me-auto mb-2 mb-lg-0">

                        <li className="nav-item">
                            <Link className="nav-link active" aria-current="page" href={'/principal'}>Principal</Link>
                        </li>

                        <li className="nav-item">
                            <Link className="nav-link active" aria-current="page" href={'/documentos'}>Documentos</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link active" aria-current="page" href={'/ventas'}>Ventas</Link>
                        </li>

                        <li className="nav-item">
                            <a className="nav-link active" href="/" onClick={salir}>
                                Cerrar Sesión
                            </a>
                        </li>

                    </ul>
                </div>
                <br />
            </div>
        </nav>
    );
}